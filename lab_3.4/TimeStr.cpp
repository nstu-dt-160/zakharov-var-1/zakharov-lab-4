#include "TimeStr.h"

TimeStr::TimeStr()
	: DateTime()
{
}

TimeStr::TimeStr(int year, int month, int day, int hour, int minute, bool pm)
	: DateTime(year, month, day, hour, minute)
{
	if (hour < 0 || hour > 12) throw std::string("incorrect hour");

	if (pm) {
		this->hour += 12;
	}
}

TimeStr::TimeStr(TimeStr& time)
	: DateTime(time)
{
}

void TimeStr::setTime(int hour, int minute, bool pm)
{
	if (hour < 0 || hour > 12) throw std::string("incorrect hour");
	
	DateTime::setTime(hour, minute);

	if (pm) {
		this->hour += 12;
	}
}

std::string TimeStr::toStr()
{
	dateTime_str->clear();

	dateTime_str->append(std::to_string(year) + '.');
	if (month < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(month) + '.');
	if (day < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(day) + ' ');

	int hour = this->hour;
	if (hour > 12) {
		hour -= 12;
	}

	if (hour < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(hour) + ':');
	if (minute < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(minute));

	dateTime_str->append(this->hour <= 12 ? " am" : " pm");

	return *dateTime_str;
}

std::ostream& operator<<(std::ostream& os, TimeStr& time)
{
	os << time.toStr();

	return os;
}

std::istream& operator>>(std::istream& is, TimeStr& time)
{
	int y, m, d, h, min, pm;

	is >> y >> m >> d >> h >> min >> pm;

	time.setDate(y, m, d);
	time.setTime(h, min, pm);

	return is;
}

std::fstream& operator<<(std::fstream& os, TimeStr& time)
{
	int hour = time.hour;
	if (hour > 12) {
		hour -= 12;
	}

	os <<
		std::to_string(time.year) << ' ' <<
		std::to_string(time.month) << ' ' <<
		std::to_string(time.day) << ' ' <<
		std::to_string(hour) << ' ' <<
		std::to_string(time.minute) << ' ' <<
		std::to_string(time.hour > 12) << '\n';

	return os;
}

std::fstream& operator>>(std::fstream& is, TimeStr& time)
{
	int y, m, d, h, min, pm;

	is >> y >> m >> d >> h >> min >> pm;

	time.setDate(y, m, d);
	time.setTime(h, min, pm);

	return is;
}