#include "DateTime.h"
#include <iostream>
#include <fstream>

bool G_DEBUG = false;

/// <summary>
/// ���������� ���� ��-��������� (2000.01.01 00:00)
/// </summary>
DateTime::DateTime()
{
	if (G_DEBUG) std::cout << "[DEBUG]: Default Constructor\n";

	dateTime_str = new std::string();

	year = 2000;
	month = 1;
	day = 1;

	hour = 0;
	minute = 0;
}

/// <summary>
/// ���������� �������� ����
/// </summary>
/// <param name="year">���</param>
/// <param name="month">�����</param>
/// <param name="day">����</param>
/// <param name="hour">���</param>
/// <param name="minute">������</param>
DateTime::DateTime(int year, int month, int day, int hour, int minute)
{
	if (G_DEBUG) std::cout << "[DEBUG]: Constructor with params\n";

	dateTime_str = new std::string();

	this->setDate(year, month, day);
	this->setTime(hour, minute);
}

/// <summary>
/// ���������� ����� ����
/// </summary>
/// <param name="dateTime">����</param>
DateTime::DateTime(const DateTime& dateTime)
{
	if (G_DEBUG) std::cout << "[DEBUG]: Copy Constructor\n";

	dateTime_str = new std::string();

	this->setDate(dateTime.year, dateTime.month, dateTime.day);
	this->setTime(dateTime.hour, dateTime.minute);
}

DateTime::~DateTime()
{
	if (G_DEBUG) std::cout << "[DEBUG]: Destructor\n";

	delete dateTime_str;
}

/// <summary>
/// ���������� ���������� ���� � ������
/// </summary>
/// <param name="year">���</param>
/// <param name="month">���������� ����� ������</param>
/// <returns></returns>
int DateTime::getDaysInMonth(int year, int month)
{
	const int _days[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }; // ������ ���������� ���� �� �������

	int daysCount = _days[month - 1];
	if (month == 2 && isLeapYear(year)) {
		daysCount++;
	}

	return daysCount;
}

/// <summary>
/// �������� �� ���������� ���
/// </summary>
/// <param name="year">���</param>
/// <returns></returns>
bool DateTime::isLeapYear(int year)
{
	if (year % 4 != 0 || year % 100 == 0 && year % 400 != 0) {
		return false;
	}

	return true;
}

/// <summary>
/// ������������� ����
/// </summary>
/// <param name="year">���</param>
/// <param name="month">�����</param>
/// <param name="day">����</param>
void DateTime::setDate(int year, int month, int day)
{
	validateDate(year, month, day);

	this->year = year;
	this->month = month;
	this->day = day;
}

/// <summary>
/// ������������� �����
/// </summary>
/// <param name="hour">���</param>
/// <param name="minute">������</param>
void DateTime::setTime(int hour, int minute)
{
	validateTime(hour, minute);
		
	this->hour = hour;
	this->minute = minute;
}

/// <summary>
/// ���������� ��������� ������������� ����
/// </summary>
/// <returns></returns>
std::string DateTime::toStr()
{
	dateTime_str->clear();

	dateTime_str->append(std::to_string(year) + '.');
	if (month < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(month) + '.');
	if (day < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(day) + ' ');

	if (hour < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(hour) + ':');
	if (minute < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(minute));

	return *dateTime_str;
}

/// <summary>
/// ������������ ���� � ������
/// </summary>
/// <param name="datetime">����</param>
/// <returns></returns>
std::string DateTime::dateTimeToStr(DateTime datetime)
{
	return datetime.toStr();
}

/// <summary>
/// ������� ���� �� �����
/// </summary>
void DateTime::print()
{
	std::cout << this->toStr() << '\n';
}

/// <summary>
/// ��������� � ���� ��������� ���������� ����. ����� ��������� ������������� ���������
/// </summary>
/// <param name="day">���������� ����</param>
/// <param name="hour">���������� �����</param>
/// <param name="minute">���������� �����</param>
/// <returns></returns>
DateTime DateTime::addInterval(int day, int hour, int minute)
{
	validateTime(abs(hour), abs(minute));

	// ���������� ��������� � ������� ����
	this->minute += minute;
	this->hour += hour;
	this->day += day;
	
	// �������� ������������ �����
	// � ����������/���������� ����
	if (this->minute < 0) {
		this->minute += 60;
		this->hour--;
	} else if (this->minute > 59) {
		this->minute -= 60;
		this->hour++;
	}

	// �������� ������������ �����
	// � ����������/���������� ���
	if (this->hour < 0) {
		this->hour += 24;
		this->day--;
	} else if (this->hour > 23) {
		this->hour -= 24;
		this->day++;
	}

	// �������� ������������ ����
	// � ����������/���������� ������, ����
	while (this->day < 1) {
		if (this->month == 1) {
			this->year--;
			this->month = 12;
			this->day += 31;
		} else {
			this->month--;
			this->day += getDaysInMonth(this->year, this->month);
		}
	}
	while (this->day > getDaysInMonth(this->year, this->month)) {
		if (this->month == 12) {
			this->year++;
			this->month = 1;
			this->day -= 31;
		} else {
			this->day -= getDaysInMonth(this->year, this->month);
			this->month++;
		}
	}

	return (*this);
}

/// <summary>
/// ���������� ����� ������ ����
/// </summary>
/// <param name="date1">���� 1</param>
/// <param name="date2">���� 2</param>
/// <returns></returns>
DateTime DateTime::minimal(DateTime date1, DateTime date2)
{
	if (date1.year < date2.year)		return date1;
	if (date1.year > date2.year)		return date2;

	if (date1.month < date2.month)		return date1;
	if (date1.month > date2.month)		return date2;

	if (date1.day < date2.day)			return date1;
	if (date1.day > date2.day)			return date2;

	if (date1.hour < date2.hour)		return date1;
	if (date1.hour > date2.hour)		return date2;

	if (date1.minute < date2.minute)	return date1;
	if (date1.minute > date2.minute)	return date2;

	return date1;
}

/// <summary>
/// ��������� ���������� ����
/// </summary>
/// <param name="year">���</param>
/// <param name="month">�����</param>
/// <param name="day">����</param>
void DateTime::validateDate(int year, int month, int day)
{
	if (year < 0 || year > 2100) throw std::string("incorrect year");
	if (month < 0 || month > 12) throw std::string("incorrect month");
	if (day < 0 || day > getDaysInMonth(year, month)) throw std::string("incorrect day");
}

/// <summary>
/// ��������� ���������� �������
/// </summary>
/// <param name="hour">���</param>
/// <param name="minute">������</param>
void DateTime::validateTime(int hour, int minute)
{
	if (hour < 0 || hour > 23) throw std::string("incorrect hour");
	if (minute < 0 || minute > 59) throw std::string("incorrect minute");
}

void DateTime::operator=(DateTime date)
{
	this->setDate(date.year, date.month, date.day);
	this->setTime(date.hour, date.minute);
}

bool DateTime::operator==(DateTime date)
{
	if (this->year == date.year &&
		this->month == date.month &&
		this->day == date.day &&
		this->hour == date.hour &&
		this->minute == date.minute) {
		return true;
	}

	return false;
}

DateTime DateTime::operator+(TimeInterval& interval)
{
	return this->addInterval(interval.day, interval.hour, interval.minute);
}

DateTime DateTime::operator+=(TimeInterval& interval)
{
	return this->operator+(interval);
}

DateTime DateTime::operator-(TimeInterval& interval)
{
	return this->addInterval(-interval.day, -interval.hour, -interval.minute);
}

DateTime DateTime::operator-=(TimeInterval& interval)
{
	return this->operator-(interval);
}

DateTime::operator const char* ()
{
	toStr();
	return (*dateTime_str).data();
}

/// <summary>
/// ���������� �������� ����� ������
/// </summary>
/// <param name="date"></param>
/// <returns></returns>
TimeInterval DateTime::operator-(DateTime& date)
{
	// ���������� TimeInterval = 0
	if (*this == date) {
		return TimeInterval();
	}

	DateTime dMin, dMax;
	bool positive = true;	// ���� ��������������� ����������
	
	// ���� ����������� � ������������ ����
	dMin = minimal(*this, date);
	if (dMin == date) {
		dMax = *this;
	}
	else {
		dMax = date;
		positive = false;
	}
	
	// ����� ���� ����� ���������� ��� ���������� ����,
	// ��������� � ������ ���� ����������� ����
	int dMinDaysCount = dMin.day,
		dMaxDaysCount = dMax.day;

	// ��������� ������ � ���
	while (dMax.month > 0) {
		dMaxDaysCount += getDaysInMonth(dMax.year, dMax.month);
		dMax.month--;
	}
	while (dMin.month > 0) {
		dMinDaysCount += getDaysInMonth(dMin.year, dMin.month);
		dMin.month--;
	}

	// ��������� ���� ������������ ���� � ���
	while (dMax.year > dMin.year) {
		dMaxDaysCount += isLeapYear(dMax.year) ? 366 : 365;
		dMax.year--;
	}

	TimeInterval interval = TimeInterval();

	// ������ �����
	interval.setTime(
		dMax.hour - dMin.hour,
		dMax.minute - dMin.minute
	);

	// ��������� ����� �� ������������
	// � ������ ����������� �������� ������� ����������
	if (interval.minute < 0) {
		interval.minute += 60;
		interval.hour--;
	}
	if (interval.hour < 0) {
		interval.hour += 24;
		interval.day--;
	}

	interval.setDay(interval.day + dMaxDaysCount - dMinDaysCount);

	if (!positive) {
		interval.day = interval.day * -1;
		interval.hour = interval.hour * -1;
		interval.minute = interval.minute * -1;
	}

	return interval;
}

std::ostream& operator<<(std::ostream& os, DateTime& date)
{
	os << date.toStr();

	return os;
}

std::istream& operator>>(std::istream& is, DateTime& date)
{
	int y, m, d, h, min;

	is >> y >> m >> d >> h >> min;

	date.setDate(y, m, d);
	date.setTime(h, min);

	return is;
}

std::fstream& operator<<(std::fstream& os, DateTime& date)
{
	os <<
		std::to_string(date.year) << ' ' <<
		std::to_string(date.month) << ' ' <<
		std::to_string(date.day) << ' ' <<
		std::to_string(date.hour) << ' ' <<
		std::to_string(date.minute) << '\n';

	return os;
}

std::fstream& operator>>(std::fstream& is, DateTime& date)
{
	int y, m, d, h, min;

	is >> y >> m >> d >> h >> min;

	date.setDate(y, m, d);
	date.setTime(h, min);

	return is;
}