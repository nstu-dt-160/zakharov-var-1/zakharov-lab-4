#pragma once
#include <string>
#include <iostream>
#include "TimeInterval.h"

extern bool G_DEBUG; // ����� ���������� ����������

class DateTime
{
	friend class TimeInterval;

public:
	DateTime();
	DateTime(int year, int month, int day, int hour, int minute);
	DateTime(const DateTime& dateTime);

	~DateTime();

	int getYear() { return year; }
	int getMonth() { return month; }
	int getDay() { return day; }
	int getHour() { return hour; }
	int getMinute() { return minute; }

	void setDate(int year, int month, int day);
	void setTime(int hour, int minute);

	void operator = (DateTime date);
	bool operator == (DateTime date);
	DateTime operator + (TimeInterval &interval);
	DateTime operator += (TimeInterval& interval);
	DateTime operator - (TimeInterval &interval);
	DateTime operator -= (TimeInterval& interval);

	operator const char* ();

	TimeInterval operator - (DateTime& date);

	friend std::ostream& operator << (std::ostream& os, DateTime& date);
	friend std::istream& operator >> (std::istream& is, DateTime& date);
	friend std::fstream& operator << (std::fstream& os, DateTime& date);
	friend std::fstream& operator >> (std::fstream& is, DateTime& date);

	std::string toStr();
	static std::string dateTimeToStr(DateTime datetime);

	void print();

protected:
	int year;
	int month;
	int day;
	int hour;
	int minute;
	std::string* dateTime_str;
	
	static int getDaysInMonth(int year, int month);
	static bool isLeapYear(int year);
	
	static void validateDate(int year, int month, int day);
	static void validateTime(int hour, int minute);

	DateTime addInterval(int day = 0, int hour = 0, int minute = 0);
	DateTime minimal(DateTime date1, DateTime date2);
};