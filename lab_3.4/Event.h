#pragma once
#include "TimeStr.h"
class Event :
    public DateTime
{
private:
    std::string eventName;

public:
    Event();
    Event(int year, int month, int day, int hour, int minute, std::string eventName);
    Event(const Event& event);

    void setEventName(std::string name) { eventName = name; }
    std::string getEventName() { return eventName; }

    std::string toStr();

    friend std::ostream& operator << (std::ostream& os, Event& event);
    friend std::istream& operator >> (std::istream& is, Event& event);
    friend std::fstream& operator << (std::fstream& os, Event& event);
    friend std::fstream& operator >> (std::fstream& is, Event& event);
};

